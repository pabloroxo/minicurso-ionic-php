import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Message } from './models/message';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WebserviceService {

  headers: Headers;
  options: RequestOptions;
  api = 'http://localhost:8000/api/';

  constructor(
    public http: Http
  ) {
    this.headers = new Headers();
    this.headers.append('Accept', 'application/json');
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.options = new RequestOptions({'headers': this.headers });
  }

  postMessage(message: Message) {
    return new Promise(resolve => {
      this.http.post(this.api + 'message', message, this.options)
        .subscribe(data => {
          resolve(data);
        });
      });
  }

  getAllMessages() {
    return new Promise(resolve => {
      this.http.get(this.api + 'message', this.options)
        .pipe(map(res => <Message>res.json()))
        .subscribe(data => {
          resolve(data);
        });
      });
  }

  getMessage(id) {
    return new Promise(resolve => {
      this.http.get(this.api + 'message/' + id, this.options)
        .pipe(map(res => <Message>res.json()))
        .subscribe(data => {
          resolve(data);
        });
      });
  }

  putMessage(message: Message) {
    return new Promise(resolve => {
      this.http.put(this.api + 'message/' + message.id, message, this.options)
        .subscribe(data => {
          resolve(data);
        });
      });
  }

  deleteMessage(id) {
    return new Promise(resolve => {
      this.http.delete(this.api + 'message/' + id, this.options)
        .subscribe(data => {
          resolve(data);
        });
      });
  }

}