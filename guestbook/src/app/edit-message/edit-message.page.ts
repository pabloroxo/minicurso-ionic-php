import { Component, OnInit } from '@angular/core';
import { WebserviceService } from '../webservice.service';
import { Message } from '../models/message';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-message',
  templateUrl: './edit-message.page.html',
  styleUrls: ['./edit-message.page.scss'],
})
export class EditMessagePage implements OnInit {

  message = new Message();

  constructor(
    public webservice: WebserviceService,
    public router: Router,
    public actvRoute: ActivatedRoute
  ) { }

  async ngOnInit() {
    await this.webservice.getMessage(this.actvRoute.snapshot.paramMap.get('id'))
      .then((message: Message) => {
        this.message = message;
      });
  }

  async submit() {
    await this.webservice.putMessage(this.message)
      .then((message: Message) => {
        this.router.navigateByUrl('/all-messages');
      });
  }

}
