<?php

namespace App\Http\Controllers;

use App\Message; 
use Illuminate\Http\Request;
 
class MessageController extends Controller
{
    public function showAll()
    {
        return response()->json(Message::all());
    }
    public function showOne($id)
    {
        return response()->json(Message::find($id));
    }
    public function create(Request $request)
    { 
         $message = Message::create($request->json()->all());  
         return response()->json($message, 201); 
    }
    public function update($id, Request $request)
    {
        $message = Message::findOrFail($id);
        $message->update($request->json()->all());
        return response()->json($message, 200); 
    }
    public function delete($id)
    {
        Message::findOrFail($id)->delete();
        return response(null, 204);
    }
}